#[derive(Clone)]
pub enum Value {
    Boolean(bool),
    Integer(i32),
    Null,
}
