use crate::{ast::AST, interpreter::Interpreter};

#[test]
fn should_print() {
    let mut interpreter = Interpreter::new();
    let ast: AST = serde_json::from_str(
        r#"{
          "Top": [
            {
              "Print": {
                "format": "hello world\\n",
                "arguments": []
              }
            }
          ]
        }"#,
    )
    .unwrap();

    interpreter.run(&ast);
}

#[test]
fn should_declare_and_access_variable() {
    let mut interpreter = Interpreter::new();
    let ast: AST = serde_json::from_str(
        r#"{
          "Top": [
            {
              "Variable": {
                "name": "x",
                "value": {
                  "Integer": 1
                }
              }
            },
            {
              "Print": {
                "format": "~\\n",
                "arguments": [
                  {
                    "AccessVariable": {
                      "name": "x"
                    }
                  }
                ]
              }
            }
          ]
        }"#,
    )
    .unwrap();

    interpreter.run(&ast);
}

#[test]
fn should_call_function() {
  let mut interpreter = Interpreter::new();
  let ast: AST = serde_json::from_str(
      r#"{
        "Top": [
          {
            "Variable": {
              "name": "x",
              "value": {
                "Integer": 1
              }
            }
          },
          {
            "Function": {
              "name": "foo",
              "parameters": [
                "x"
              ],
              "body": {
                "Print": {
                  "format": "~\\n",
                  "arguments": [
                    {
                      "AccessVariable": {
                        "name": "x"
                      }
                    }
                  ]
                }
              }
            }
          },
          {
            "CallFunction": {
              "name": "foo",
              "arguments": [
                {
                  "Integer": 2
                }
              ]
            }
          },
          {
            "Print": {
              "format": "~\\n",
              "arguments": [
                {
                  "AccessVariable": {
                    "name": "x"
                  }
                }
              ]
            }
          }
        ]
      }"#,
  )
  .unwrap();

  interpreter.run(&ast);
}

#[test]
fn should_eval_conditional() {
  let mut interpreter = Interpreter::new();
  let ast: AST = serde_json::from_str(
      r#"{
        "Top": [
          {
            "Conditional": {
              "condition": {
                "Boolean": true
              },
              "consequent": {
                "Print": {
                  "format": "true branch\\n",
                  "arguments": []
                }
              },
              "alternative": {
                "Print": {
                  "format": "false branch\\n",
                  "arguments": []
                }
              }
            }
          },
          {
            "Conditional": {
              "condition": {
                "Boolean": false
              },
              "consequent": {
                "Print": {
                  "format": "true branch\\n",
                  "arguments": []
                }
              },
              "alternative": {
                "Print": {
                  "format": "false branch\\n",
                  "arguments": []
                }
              }
            }
          }
        ]
      }"#,
  )
  .unwrap();

  interpreter.run(&ast);
}