use std::collections::HashMap;

use crate::heap::HeapPointer;

pub enum StackFrame<'a> {
    Empty,
    Frame {
        variables: HashMap<String, HeapPointer>,
        previous: &'a StackFrame<'a>,
    },
}

impl<'a> StackFrame<'a> {
    pub fn new_frame(previous: &'a StackFrame) -> StackFrame<'a> {
        StackFrame::Frame {
            variables: HashMap::new(),
            previous,
        }
    }

    pub fn find_variable_heap_pointer(&self, variable_name: &String) -> Option<HeapPointer> {
        match self {
            StackFrame::Empty => None,
            StackFrame::Frame {
                variables,
                previous,
            } => match variables.get(variable_name) {
                Some(heap_pointer) => Some(*heap_pointer),
                None => previous.find_variable_heap_pointer(variable_name),
            },
        }
    }

    pub fn set_variable_heap_pointer(&mut self, variable_name: &String, heap_pointer: HeapPointer) {
        if let StackFrame::Frame { variables, .. } = self {
            if let Some(_) = variables.insert(variable_name.clone(), heap_pointer) {
                panic!("Variable `{}` is already defined", variable_name)
            }
        }
    }

    pub fn set_previous(&mut self, new_previous: &'a StackFrame<'a>) {
        if let StackFrame::Frame { previous, .. } = self {
            *previous = new_previous;
        };
    }
}
