use serde::Deserialize;

// NOTE: taken from FML refrence because it already uses serde and I was too lazy to write all of this
#[derive(Deserialize, Clone)]
pub enum AST {
    Null,
    Boolean(bool),
    Integer(i32),

    Variable {
        name: String,
        value: Box<AST>,
    },

    AccessVariable {
        name: String,
    },

    AssignVariable {
        name: String,
        value: Box<AST>,
    },

    Function {
        name: String,
        parameters: Vec<String>,
        body: Box<AST>,
    },

    CallFunction {
        name: String,
        arguments: Vec<Box<AST>>,
    },

    Top(Vec<Box<AST>>),
    Block(Vec<Box<AST>>),
    Loop {
        condition: Box<AST>,
        body: Box<AST>,
    },
    Conditional {
        condition: Box<AST>,
        consequent: Box<AST>,
        alternative: Box<AST>,
    },

    Print {
        format: String,
        arguments: Vec<Box<AST>>,
    },
}
