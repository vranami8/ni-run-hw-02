use crate::ast::AST;

pub struct Function {
    pub parameters: Vec<String>,
    pub body: Box<AST>,
}

impl Function {
    pub fn new(parameters: Vec<String>, body: Box<AST>) -> Self {
        Function { parameters, body }
    }
}
