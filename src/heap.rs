use crate::value::Value;

pub type HeapPointer = usize;

pub struct Heap {
    memory: Vec<Value>,
}

impl Heap {
    pub fn new() -> Self {
        Heap { memory: Vec::new() }
    }

    pub fn allocate(& mut self, value: Value) -> HeapPointer {
        self.memory.push(value);
        self.memory.len() - 1
    }

    pub fn get(&self, heap_pointer: HeapPointer) -> &Value {
        match self.memory.get(heap_pointer) {
            Some(heap_object) => heap_object,
            None => panic!("Invalid heap pointer"),
        }
    }

    pub fn set(& mut self, heap_pointer: HeapPointer, heap_object: Value) {
        let heap_ref = match self.memory.get_mut(heap_pointer) {
            Some(it) => it,
            None => panic!("Invalid heap pointer"),
        };

        *heap_ref = heap_object
    }
}
