use std::io;

use interpreter::Interpreter;

use crate::ast::AST;

#[cfg(test)]
mod tests;

mod ast;
mod stack;
mod function;
mod heap;
mod interpreter;
mod value;

fn main() {
    let ast: AST = serde_json::from_reader(io::stdin())
        .unwrap_or_else(|err| panic!("JSON parsing error: {}", err.to_string()));

    let mut interpreter = Interpreter::new();
    interpreter.run(&ast);
}
