use std::{collections::HashMap};
use unescape::unescape;

use crate::{ast::AST, stack::StackFrame, function::Function, heap::Heap, value::Value};

type FunctionMap = HashMap<String, Function>;

pub struct Interpreter {
    heap: Heap,
}

impl Interpreter {
    pub fn new() -> Self {
        Interpreter { heap: Heap::new() }
    }

    pub fn run(&mut self, ast: &AST) {
        match ast {
            AST::Top(expressions) => self.eval_top(expressions),
            _ => panic!("Root AST node is not a `Top`"),
        }
    }

    fn eval_top(&mut self, expressions: &Vec<Box<AST>>) {
        let mut stack = StackFrame::new_frame(&StackFrame::Empty);
        let mut functions = FunctionMap::new();

        for expression in expressions {
            match expression.as_ref() {
                AST::Function {
                    name,
                    parameters,
                    body,
                } => self.declare_function(&mut functions, name, parameters, body),
                _ => self.eval(&mut stack, &functions, expression),
            };
        }
    }

    fn eval(
        &mut self,
        stack: &mut StackFrame,
        functions: &FunctionMap,
        expression: &Box<AST>,
    ) -> Value {
        match expression.as_ref() {
            AST::Null => Value::Null,
            AST::Boolean(value) => Value::Boolean(*value),
            AST::Integer(value) => Value::Integer(*value),
            AST::Variable { name, value } => self.declare_variable(stack, functions, name, value),
            AST::AccessVariable { name } => self.access_variable(stack, name),
            AST::AssignVariable { name, value } => {
                self.assign_variable(stack, functions, name, value)
            }
            AST::CallFunction { name, arguments } => {
                self.call_function(stack, functions, name, arguments)
            }
            AST::Block(expressions) => self.eval_expressions(stack, functions, expressions),
            AST::Loop { condition, body } => self.eval_loop(stack, functions, condition, body),
            AST::Conditional {
                condition,
                consequent,
                alternative,
            } => self.eval_conditional(stack, functions, condition, consequent, alternative),
            AST::Print { format, arguments } => {
                self.eval_print(stack, functions, format, arguments)
            }
            _ => panic!("Invalid AST node"),
        }
    }

    fn eval_expressions(
        &mut self,
        stack: &mut StackFrame,
        functions: &FunctionMap,
        expressions: &Vec<Box<AST>>,
    ) -> Value {
        if expressions.len() > 1 {
            for expression in expressions.iter().take(expressions.len() - 1) {
                self.eval(stack, functions, expression);
            }
        }

        match expressions.last() {
            Some(expression) => self.eval(stack, functions, expression),
            None => Value::Null,
        }
    }

    fn declare_variable(
        &mut self,
        stack: &mut StackFrame,
        functions: &FunctionMap,
        name: &String,
        value_expression: &Box<AST>,
    ) -> Value {
        let value = self.eval(stack, functions, value_expression);
        let heap_pointer = self.heap.allocate(value.clone());
        stack.set_variable_heap_pointer(name, heap_pointer);

        value
    }

    fn access_variable(&self, stack: &mut StackFrame, variable_name: &String) -> Value {
        match stack.find_variable_heap_pointer(variable_name) {
            Some(heap_pointer) => self.heap.get(heap_pointer).clone(),
            None => panic!("Undeclared variable `{}`", variable_name),
        }
    }

    fn assign_variable(
        &mut self,
        stack: &mut StackFrame,
        functions: &FunctionMap,
        variable_name: &String,
        value_expression: &Box<AST>,
    ) -> Value {
        let value = self.eval(stack, functions, value_expression);

        let heap_pointer = match stack.find_variable_heap_pointer(variable_name) {
            Some(pointer) => pointer,
            None => panic!(
                "Can not assign a value to an undeclared variable `{}`",
                variable_name
            ),
        };

        self.heap.set(heap_pointer, value.clone());

        value
    }

    fn declare_function(
        &mut self,
        functions: &mut FunctionMap,
        function_name: &String,
        parameters: &Vec<String>,
        body: &Box<AST>,
    ) -> Value {
        if let Some(_) = functions.insert(
            function_name.clone(),
            Function::new(parameters.clone(), body.clone()),
        ) {
            panic!("Function `{}` has already been declared", function_name)
        };

        Value::Null
    }

    fn call_function(
        &mut self,
        stack: &mut StackFrame,
        functions: &FunctionMap,
        name: &String,
        arguments: &Vec<Box<AST>>,
    ) -> Value {
        let function = functions
            .get(name)
            .unwrap_or_else(|| panic!("Can not call undeclared function `{}`", name));

        if function.parameters.len() != arguments.len() {
            panic!(
                "Function `{}` requires {} arguments, but {} were provided",
                name,
                function.parameters.len(),
                arguments.len()
            )
        }

        let mut new_stack = StackFrame::new_frame(&StackFrame::Empty);

        for (argument, parameter) in arguments.iter().zip(&function.parameters) {
            let argument_value = self.eval(stack, functions, argument);
            let heap_pointer = self.heap.allocate(argument_value);
            new_stack.set_variable_heap_pointer(parameter, heap_pointer);
        }

        new_stack.set_previous(stack);

        self.eval(&mut new_stack, functions, &function.body)
    }

    fn eval_loop(
        &mut self,
        stack: &mut StackFrame,
        functions: &FunctionMap,
        condition: &Box<AST>,
        body: &Box<AST>,
    ) -> Value {
        while match self.eval(stack, functions, condition) {
            Value::Boolean(value) => value,
            _ => panic!("Loop expression condition did not evaluate to boolean"),
        } {
            self.eval(stack, functions, body);
        }

        Value::Null
    }

    fn eval_conditional(
        &mut self,
        stack: &mut StackFrame,
        functions: &FunctionMap,
        condition: &Box<AST>,
        consequent: &Box<AST>,
        alternative: &Box<AST>,
    ) -> Value {
        match self.eval(stack, functions, condition) {
            Value::Boolean(true) => self.eval(stack, functions, consequent),
            Value::Boolean(false) => self.eval(stack, functions, alternative),
            _ => panic!("if expression condition did not evaluate to boolean"),
        }
    }

    fn eval_print(
        &mut self,
        stack: &mut StackFrame,
        functions: &FunctionMap,
        format: &String,
        arguments: &Vec<Box<AST>>,
    ) -> Value {
        let unescaped_format = unescape(format).unwrap();
        let strings: Vec<&str> = unescaped_format.split("~").collect();

        for (string, argument) in strings.iter().zip(arguments) {
            print!("{}", string);

            match self.eval(stack, functions, argument) {
                Value::Boolean(value) => print!("{}", value),
                Value::Integer(value) => print!("{}", value),
                Value::Null => print!("null"),
            }
        }

        if let Some(string) = strings.last() {
            print!("{}", string)
        }

        Value::Null
    }
}
